package greetings

import (
	"errors"
	"fmt"
	"math/rand"
)

func Hello(name string) (string, error) {
	if name == "" {
		// return "",errors.New("empty name")
		return "", errors.New("empty name")
	}
	message := fmt.Sprintf(RandomFormat(), name)
	//message := fmt.Sprintf(RandomFormat())
	return message, nil
}

func Hellos(names []string) (map[string]string, error) {

	messages := make(map[string]string)
	for _, name := range names {
		// fmt.Printf("索引:%d \n",index)
		message, err := Hello(name)
		if err != nil {
			return nil, err
		}
		messages[name] = message
	}
	return messages, nil
}

func RandomFormat() string {
	formats := []string{
		"Hi, %v. welcome.",
		"Great to seet you, %v",
		"Hail,%v! well met!",
	}
	// fmt.Printf("随机数: %d \n", rand.Intn(9))
	return formats[rand.Intn(len(formats))]
}

func randomFormat() string {
	formats := []string{
		"Hi, %v. welcome.",
		"Great to seet you, %v",
		"Hail,%v! well met!",
	}
	fmt.Printf("随机数: %d \n", rand.Intn(9))
	return formats[rand.Intn(len(formats))]
}
